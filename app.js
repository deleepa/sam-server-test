var express = require('express');
var azureMobileApss = require('azure-mobile-apps');

var app = express();
var mobile  = azureMobileApss();

mobile.tables.import('./tables');

mobile.tables.initialize()
            .then(function() {
                app.use(mobile);
                app.listen(process.env.PORT || 3000);
            });

