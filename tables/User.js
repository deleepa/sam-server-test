var azureMobileApps = require('azure-mobile-apps');

var table= azureMobileApps.table();

table.columns = {
    'email': 'string',
    'password': 'string'
};

table.seed = [
    { email: 'johnwick@gmail.com', password: 'password'},
    { email: 'austinpowers@gmail.com', password: 'password'},
];

table.dynamicSchema = false;

console.log("user table has been created!");

module.exports = table;